FROM python:alpine3.13

WORKDIR /app/

COPY requirements.txt /app/
RUN pip install -r requirements.txt

COPY . /app/

CMD /bin/sh -c "gunicorn -w 4 -b 0.0.0.0:$PORT backend:app"