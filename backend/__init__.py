from flask import Flask
app = Flask(__name__)

# Enable CORS with cookies for all routes
from flask_cors import CORS
CORS(app, supports_credentials=True)

# Wrap flask request with db_session
from pony.flask import Pony
Pony(app)

# Don't redirect objects to directories
app.url_map.strict_slashes = False

from werkzeug.exceptions import UnsupportedMediaType
from functools import wraps
from flask import request
def jsonify_body(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if request.headers.get('Content-Type') != 'application/json':
            raise UnsupportedMediaType()
        data = request.get_json()
        return func(*args, **kwargs, data=data)
    return wrapper

from werkzeug.exceptions import HTTPException
@app.errorhandler(HTTPException)
def handle_http_exception(e):
    return {'message': e.description}, e.code

import backend.models
import backend.views