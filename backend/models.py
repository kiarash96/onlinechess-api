from datetime import datetime, timedelta
from pony.orm import *

STARTING_BOARD = [5, 2, 6, 4, 1, 6, 2, 5,
                  3, 3, 3, 3, 3, 3, 3, 3,
                  0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0,
                  9, 9, 9, 9, 9, 9, 9, 9,
                  11, 8, 12, 10, 7, 12, 8, 11]

db = Database()

class User(db.Entity):
    id = PrimaryKey(int, auto=True)
    username = Required(str, unique=True)
    password = Required(str)
    last_seen = Optional(datetime)
    host_games = Set('Game', reverse='host')
    guest_games = Set('Game', reverse='guest')
    
    def to_dict(self):
        db.commit()
        return {
            'id': self.id,
            'username': self.username,
            'status': 'online' if self.last_seen and datetime.now() - self.last_seen < timedelta(minutes=5) else 'offline'
        }

class Game(db.Entity):
    id = PrimaryKey(int, auto=True)
    host = Required(User)
    guest = Required(User)
    board = Required(IntArray, default=STARTING_BOARD)
    
    def to_dict(self):
        db.commit()
        return {
            'id': self.id,
            'host': self.host.to_dict(),
            'guest': self.guest.to_dict(),
            'board': self.board
        }

db.bind(provider='sqlite', filename='database.db', create_db=True)
db.generate_mapping(create_tables=True)