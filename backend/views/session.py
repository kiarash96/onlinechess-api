import os, base64, json
from functools import wraps
from datetime import datetime, timedelta

from werkzeug.exceptions import Unauthorized
from flask import request, make_response
from flask.views import MethodView

from backend import models, app, jsonify_body

def encode(session):
    return base64.b64encode(json.dumps(session).encode()).decode()

def decode(encoded_session):
    try:
        return json.loads(base64.b64decode(encoded_session.encode()).decode())
    except:
        return None
    
def set_current_session(resp, session):
    resp.set_cookie('session', encode(session))

def get_current_session():
    return decode(request.cookies.get('session'))

def get_current_user():
    session = get_current_session()
    if not session:
        return None
    return models.User.get(id=session.get('id'), password=session.get('password'))

def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if not get_current_user():
            raise Unauthorized()
        return func(*args, **kwargs)
    return wrapper
    
class Session(MethodView):
    @jsonify_body
    def put(self, data):
        u = models.User.get(username=data['user']['username'], password=data['user']['password'])
        if not u:
            raise Unauthorized('Invalid username/password')

        u.last_seen = datetime.now()
        
        session = {'id': u.id, 'password': u.password}
        resp = make_response({})
        set_current_session(resp, session)
        return resp, 201
    
    @login_required
    def get(self):
        return {}
    
    @login_required
    def delete(self):
        # Make user offline
        u = get_current_user()
        u.last_seen = datetime.now() - timedelta(minutes=5)

        resp = make_response({})
        set_current_session(resp, {})
        return resp
        
app.add_url_rule('/session', view_func=Session.as_view('Session'))

@app.before_request
def update_last_seen():
    u = get_current_user()
    if u:
        u.last_seen = datetime.now()