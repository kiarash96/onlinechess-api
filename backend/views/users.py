from werkzeug.exceptions import Conflict, NotFound
from flask import jsonify
from flask.views import MethodView
from pony import orm

from backend import app, models, jsonify_body

class UsersList(MethodView):
    def get(self):
        users = orm.select(x for x in models.User)[:]
        return jsonify([x.to_dict() for x in users])

    @jsonify_body
    def post(self, data):
        if models.User.get(username=data['username']):
            raise Conflict('A user with this username already exists.')
        x = models.User(username=data['username'], password=data['password'])
        return x.to_dict(), 201
    
class User(MethodView):
    def get(self, id):
        u = models.User.get(id=id)
        if not u:
            raise NotFound()
        return u.to_dict()
        
app.add_url_rule('/users', view_func=UsersList.as_view('UsersList'))
app.add_url_rule('/users/<id>', view_func=User.as_view('User'))