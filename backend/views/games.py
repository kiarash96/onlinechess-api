from werkzeug.exceptions import NotFound, BadRequest, Conflict
from flask import request, jsonify
from flask.views import MethodView
from pony import orm

from backend import models, app, jsonify_body
from backend.views.session import login_required, get_current_user

class GamesList(MethodView):
    def get(self):
        games = orm.select(x for x in models.Game)[:]
        return jsonify([x.to_dict() for x in games])

    @login_required
    @jsonify_body
    def post(self, data):
        g_username = data['guest'].get('username')
        if not g_username:
            raise BadRequest('Request did not specify guest player.')
        guest = models.User.get(username=g_username)
        if not guest:
            raise NotFound('Guest user does not exist.')

        host = get_current_user()

        g = models.Game(host=host, guest=guest)

        return g.to_dict(), 201

class Game(MethodView):
    def get(self, id):
        g = models.Game.get(id=id)
        if not g:
            raise NotFound()
        return g.to_dict()
    
class MovesList(MethodView):
    @login_required
    @jsonify_body
    def post(self, id, data):
        g = models.Game.get(id=id)
        if not g:
            raise NotFound()

        # TODO: Check host/guest permission + Turn 
        # TODO: Check if move is allowed
        
        i1 = int(data['from'])
        i2 = int(data['to'])
        
        g.board[i1], g.board[i2] = g.board[i2], g.board[i1]

        return {}, 201

app.add_url_rule('/games', view_func=GamesList.as_view('GamesList'))
app.add_url_rule('/games/<id>', view_func=Game.as_view('Game'))
app.add_url_rule('/games/<id>/moves', view_func=MovesList.as_view('MovesList'))
